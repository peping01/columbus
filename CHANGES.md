## More samples release ##

### Whats new: ###
   **NOTE**: This is an Android samples only release, library version and Showcase app (sampleapp) stay the same)
   - Improved ConnectivityAPISample
   - Added SensorSample
   - Added ECGSample


## Android Library Version: 1.6.1 ##
## iOS Library Version: 1.3.1 ##
## Application Version: 1.3 ##

### Whats new: ###
   **NOTE**: This is an Android only release (i.e. the changes are only about the Android version)
   - Improve DFU update process ( fix issue with device disconnected error)
   - Add apk with Android sample App in directory android/samples/sampleapp/
   - Fix 404 error after changing device name
   - Add support for dfu manufacture data ( fix issue with unknown macAddress when Movesense has manufacture data writen)
   - Fix typo in Magnetic Field
   - Fix null serial and sw version after device name change
   - Add information about connected device into main screen after connection
   - Fix dfu incrementation with
   - Fix connection issue with fast stop scanning click before connection was established
   
## Android Library Version: 1.6.0 ##
## iOS Library Version: 1.3.1 ##
## Application Version: 1.2 ##

### Whats new: ###
   **NOTE**: This is an Android only release (i.e. the changes are only about the Android version)
   - Moved the Android samples under separate folder
   - The .aar package can now be found in folder *./Android/Movesense/*
   - New simple *Connectivity API* for Android MDS library
   - New sample for Android: "ConnectivityAPISample"
   - NOTE: The *sampleapp* still uses the old connectivity

   (No known bugs since half of the samples use still the old method)

## Library Version: 1.3.1 ##
## Application Version: 1.2 ##

### Whats new: ###

   - Improvment UI design
   - DFU is moved to the main screen
   - Added MultiConnection
   - Added Version Library and Application on the main view
   - Toolbars changing name to current view

### Known Bugs: ###

   - Movesense not reconnecting with app after bluetooth off / on.
   - Nexus 6 can't connect with Movesense ( Android platform BUG )

## Library Version: 1.3.1 ##
## Application Version: 1.1 ##

### Whats new: ###

   - Fixed: Memory leaks for longer sensor subscriptions ( BackPressureException ) 
   - Recovery for non-intentionall disconnect ( Movesense out-of-range / disconnect / connection lost )
   - Validation for DFU file ( sometimes file was corrupted by Google Drive )
   - Subscription is not stopped when devices is going to sleep.
   - Allow connection for new and old software ( changes in JSON response)
   - More Unit Tests for library.
   
### Known Bugs: ###

   - When DFU update will fail at some point then may have problem with connection because Movesense is still in DFU mode ( solution -> DFU will be moved to main view in next Application version )
   - Movesense sometimes can't reconnect to Application.
   - Movesense not reconnecting with app after bluetooth off / on.
