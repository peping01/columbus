package com.movesense.mds.sampleapp.example_app_using_mds_api.tests;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.api.client.json.Json;
import com.google.gson.Gson;
import com.movesense.mds.Mds;
import com.movesense.mds.MdsException;
import com.movesense.mds.MdsNotificationListener;
import com.movesense.mds.MdsSubscription;
import com.movesense.mds.sampleapp.BleManager;
import com.movesense.mds.sampleapp.ConnectionLostDialog;
import com.movesense.mds.sampleapp.R;
import com.movesense.mds.sampleapp.example_app_using_mds_api.FormatHelper;
import com.movesense.mds.sampleapp.example_app_using_mds_api.logs.LogsManager;
import com.movesense.mds.sampleapp.example_app_using_mds_api.model.HeartRate;
import com.movesense.mds.sampleapp.example_app_using_mds_api.model.MovesenseConnectedDevices;
import com.polidea.rxandroidble.RxBleDevice;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConnectedHtml5Activity extends AppCompatActivity implements BleManager.IBleConnectionMonitor {
    private final String LOG_TAG = ConnectedHtml5Activity.class.getSimpleName();

    @BindView(R.id.connected_html5_webView)
    WebView webView;
    private LogsManager logsManager;
    private MdsSubscription currentSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connected_html5);
        ButterKnife.bind(this);

        initWebView();

        logsManager = new LogsManager(this);

        BleManager.INSTANCE.addBleConnectionMonitorListener(this);
    }

    private void initWebView() {
        // Open all links in the same WebView
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(request.getUrl().toString());
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                beginHeartRateCapture();
            }
        });

        // Enable Javascript and file access for WebViews
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccess(true);
        settings.setAllowUniversalAccessFromFileURLs(true);

        webView.loadUrl("https://suunto-columbus.herokuapp.com/");
    }

    private void beginHeartRateCapture() {
        unsubscribe();
        currentSubscription = Mds.builder().build(this).subscribe(
                "suunto://MDS/EventListener",
                FormatHelper.formatContractToJson(MovesenseConnectedDevices.getConnectedDevice(0).getSerial(),
                        "Meas/Hr"),
                new MdsNotificationListener() {
                    @Override
                    public void onNotification(String data) {
                        Log.d(LOG_TAG, "Heart rate onNotification() : " + data);
                        HeartRate heartRate = new Gson().fromJson(data, HeartRate.class);

                        if (heartRate != null) {
                            Double heartRatePerMinute = heartRate.body.average;
                            webView.evaluateJavascript(
                                String.format("dispatch(actions.setHeartRate(%.2f))", heartRatePerMinute),
                                null
                            );
                        }
                    }

                    @Override
                    public void onError(MdsException error) {
                        Log.e(LOG_TAG, "Heart rate error", error);
                    }
                }
        );
    }

    private void unsubscribe() {
        if (currentSubscription != null) {
            currentSubscription.unsubscribe();
            currentSubscription = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unsubscribe();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        BleManager.INSTANCE.removeBleConnectionMonitorListener(this);
    }

    @Override
    public void onDisconnect(RxBleDevice rxBleDevice) {
        Log.e(LOG_TAG, "onDisconnect: " + rxBleDevice.getName() + " " + rxBleDevice.getMacAddress());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ConnectionLostDialog.INSTANCE.showDialog(ConnectedHtml5Activity.this);
            }
        });
    }

    @Override
    public void onConnect(RxBleDevice rxBleDevice) {
        Log.e(LOG_TAG, "onConnect: " + rxBleDevice.getName() + " " + rxBleDevice.getMacAddress());
    }
}
