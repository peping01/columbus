package com.movesense.mds.sampleapp.example_app_using_mds_api.about;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.movesense.mds.sampleapp.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
