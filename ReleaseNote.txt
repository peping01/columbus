1.7
    - WB 3.7.0
    - Fix occasional iOS BLE reconnection failure

1.6
    - Add Android BLE connectivity API
    - Add path parameter support to MDS Server API
    - Fix occasional timeout errors
    - Fix multiple query parameter syntax
    - Fix MDS/Logbook serial handling and out-of-range accessing

1.5
    - WB 3.6.1
    - WB resource registration support to MDS API

1.4
    - WB 3.6
    - Increment filesystem read timeout
    - Fix query parameter support for whiteboard requests
    - Remove invalid descriptors from MDS/Logbook cache

1.3.1
    - MDS/Logbook-resource to support for Movesense API
    - Fix crash when BLE is disabled during data transfer from device to MDS
    - Fix crash when notification is handled during MDS destruction
    - (iOS) After deactivate() call methods returns an error
    - Fix SBEM array handling
    - Whiteboard 3.4.1 

0.0.1 (iOS only release):

    - Support for connecting to Movesense sensor
    - Support for GET
    - Support for subscribe/unsubscribe
